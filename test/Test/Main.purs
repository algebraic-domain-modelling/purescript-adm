module Test.Main where

import Test.Adm.Adm as Adm
import Test.Adm.Html as Html
import Test.Adm.Parser as Parser
import Test.Adm.View as View

import Prelude

-- aff
import Effect.Aff (Fiber, launchAff)

-- effect
import Effect (Effect)

-- spec
import Test.Spec.Reporter.Console (consoleReporter)
import Test.Spec.Runner (runSpec)

main :: Effect (Fiber Unit)
main = launchAff $ runSpec [consoleReporter] do
  Adm.suite
  Html.suite
  Parser.suite
  View.suite
