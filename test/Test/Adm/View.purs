module Test.Adm.View where

import Adm.Adm (Adm(..))
import Adm.View (AdmView(..), view)

import Prelude

-- spec
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (shouldEqual)

suite :: Spec Unit
suite = describe "view" do
  it "maps a Name to a VName" do
    view (Name "Marco") `shouldEqual` (VName "Marco")
  it "maps a Hole to a VHole" do
    view Hole `shouldEqual` VHole
  it "maps a Unit to a VUnit" do
    view Unit `shouldEqual` VUnit
  it "maps a Sum to a VSum" do
    view (Sum (Name "Marco") (Name "Paolo")) `shouldEqual` (VSum [VName "Marco", VName "Paolo"])
  it "maps a Sum of sums to a VSum" do
    view (Sum (Sum (Name "Marco") (Name "Paolo")) (Sum (Name "Cecilia") (Name "Greta")))
      `shouldEqual` (VSum [VName "Marco", VName "Paolo", VName "Cecilia", VName "Greta"])
  it "maps a nested Sum to a VSum" do
    view (Sum (Sum (Sum (Name "Marco") (Name "Paolo")) (Name "Cecilia")) (Name "Greta"))
      `shouldEqual` (VSum [VName "Marco", VName "Paolo", VName "Cecilia", VName "Greta"])
  it "maps a Product to a VProduct" do
    view (Product (Name "Marco") (Name "Paolo")) `shouldEqual` (VProduct [VName "Marco", VName "Paolo"])
  it "maps a Product of products to a VProduct" do
    view (Product (Product (Name "Marco") (Name "Paolo")) (Product (Name "Cecilia") (Name "Greta")))
      `shouldEqual` (VProduct [VName "Marco", VName "Paolo", VName "Cecilia", VName "Greta"])
  it "maps a nested Product to a VProduct" do
    view (Product (Product (Product (Name "Marco") (Name "Paolo")) (Name "Cecilia")) (Name "Greta"))
      `shouldEqual` (VProduct [VName "Marco", VName "Paolo", VName "Cecilia", VName "Greta"])
  it "maps a Function to a VFunction" do
    view (Function (Name "Marco") (Name "Paolo")) `shouldEqual` (VFunction (VName "Marco") (VName "Paolo"))
  it "maps a Sequence to a VSequence" do
    view (Sequence (Name "Marco") (Name "Paolo")) `shouldEqual` (VSequence [VName "Marco", VName "Paolo"])
  it "maps a Sequence of sequences to a VSequence" do
    view (Sequence (Sequence (Name "Marco") (Name "Paolo")) (Sequence (Name "Cecilia") (Name "Greta")))
      `shouldEqual` (VSequence [VName "Marco", VName "Paolo", VName "Cecilia", VName "Greta"])
  it "maps a nested Sequence to a VSequence" do
    view (Sequence (Sequence (Sequence (Name "Marco") (Name "Paolo")) (Name "Cecilia")) (Name "Greta"))
      `shouldEqual` (VSequence [VName "Marco", VName "Paolo", VName "Cecilia", VName "Greta"])
  it "maps a Wrap to a VWrap" do
    view (Wrap "Cecilia" (Name "Paolo")) `shouldEqual` (VWrap "Cecilia" (VName "Paolo"))