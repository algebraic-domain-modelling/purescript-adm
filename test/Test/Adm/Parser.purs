module Test.Adm.Parser where

import Adm.Adm (Adm(..), Assignment(..))
import Adm.Parser (admParser, assignmentAdmParser, multipleAssignmentsAdmParser)

import Prelude

-- either
import Data.Either (Either(..))

-- exceptions
import Effect.Exception (Error)

-- lists
import Data.List.Types (List(..), NonEmptyList(..))

-- nonempty
import Data.NonEmpty (NonEmpty(..))

-- spec
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (shouldEqual)

-- string-parsers
import Text.Parsing.StringParser (ParseError(..), runParser)

-- transformers
import Control.Monad.Error.Class (class MonadThrow)

parseAdm :: forall m. MonadThrow Error m => String -> Either ParseError Adm -> m Unit
parseAdm s admResult = runParser admParser s `shouldEqual` admResult

suite :: Spec Unit
suite = describe "Parser" do
  describe "Name parser" do
    it "parses a string" do
      parseAdm "Hello" (Right (Name "Hello"))
    it "parses a string wrapped in parentheses" do
      parseAdm "(Hello)" (Right (Name "Hello"))
    it "does not parse a string followed by an = sign" do
      parseAdm "Hello =" (Left (ParseError "Negated parser succeeded"))

  describe "Hole parser" do
    it "parses an underscore" do
      parseAdm "_" (Right Hole)

  describe "Unit parser" do
    it "parses a 1" do
      parseAdm "1" (Right Unit)

  describe "Wrap parser" do
    it "parses two strings separated by a whitespace" do
      parseAdm "Hello Marco" (Right (Wrap "Hello" (Name "Marco")))
    it "parses a string followed by a parenthesised adm" do
      parseAdm "Hello (Marco + Paolo)" (Right (Wrap "Hello" (Sum (Name "Marco") (Name "Paolo"))))
    it "parses a summed wrapped adm" do
      parseAdm "Hello Marco + Paolo" (Right (Sum (Wrap "Hello" (Name "Marco")) (Name "Paolo")))

  describe "Sum parser" do
    it "parses a sum of two names" do
      parseAdm "Marco + Paolo" (Right (Sum (Name "Marco") (Name "Paolo")))
    it "parses a sum of three names" do
      parseAdm "Marco + Paolo + Cecilia" (Right (Sum (Name "Marco") (Sum (Name "Paolo") (Name "Cecilia"))))
    it "parses a parenthesised sum of three names" do
      parseAdm "(Marco + Paolo) + Cecilia" (Right (Sum (Sum (Name "Marco") (Name "Paolo")) (Name "Cecilia")))

  describe "Product parser" do
    it "parses a product of two names" do
      parseAdm "Marco * Paolo" (Right (Product (Name "Marco") (Name "Paolo")))
    it "parses a product of three names" do
      parseAdm "Marco * Paolo * Cecilia" (Right (Product (Name "Marco") (Product (Name "Paolo") (Name "Cecilia"))))
    it "parses a parenthesised product of three names" do
      parseAdm "(Marco * Paolo) * Cecilia" (Right (Product (Product (Name "Marco") (Name "Paolo")) (Name "Cecilia")))

  describe "Function parser" do
    it "parses a function between two names" do
      parseAdm "Marco -> Paolo" (Right (Function (Name "Marco") (Name "Paolo")))
    it "parses a curried function with two inputs" do
      parseAdm "Marco -> Paolo -> Cecilia" (Right (Function (Name "Marco") (Function (Name "Paolo") (Name "Cecilia"))))
    it "parses a function with a function as input" do
      parseAdm "(Marco -> Paolo) -> Cecilia" (Right (Function (Function (Name "Marco") (Name "Paolo")) (Name "Cecilia")))

  describe "Sequence parser" do
    it "parses a sequence between two names" do
      parseAdm "Marco ; Paolo" (Right (Sequence (Name "Marco") (Name "Paolo")))
    it "parses a sequence with three items" do
      parseAdm "Marco ; Cecilia ; Paolo" (Right (Sequence (Name "Marco") (Sequence (Name "Cecilia") (Name "Paolo"))))
    it "parses a parenthesised sequence of three names" do
      parseAdm "(Marco ; Cecilia) ; Paolo" (Right (Sequence (Sequence (Name "Marco") (Name "Cecilia")) (Name "Paolo")))

  describe "Adm parser" do
    it "parses a sum of products" do
      parseAdm "Marco * Paolo + Cecilia * Greta" (Right (Sum (Product (Name "Marco") (Name "Paolo")) (Product (Name "Cecilia") (Name "Greta"))))
    it "parses a produt of sums" do
      parseAdm "Marco * (Paolo + Cecilia) * Greta" (Right (Product (Name "Marco") (Product (Sum (Name "Paolo") (Name "Cecilia")) (Name "Greta"))))
    it "parses a function between sums" do
      parseAdm "Marco + Paolo -> Cecilia + Greta" (Right (Function (Sum (Name "Marco") (Name "Paolo")) (Sum (Name "Cecilia") (Name "Greta"))))
    it "parses a sum of functions" do
      parseAdm "(Marco -> Paolo) + (Cecilia -> Greta)" (Right (Sum (Function (Name "Marco") (Name "Paolo")) (Function (Name "Cecilia") (Name "Greta"))))
    it "parses a function between products" do
      parseAdm "Marco * Paolo -> Cecilia * Greta" (Right (Function (Product (Name "Marco") (Name "Paolo")) (Product (Name "Cecilia") (Name "Greta"))))
    it "parses a product of functions" do
      parseAdm "(Marco -> Paolo) * (Cecilia -> Greta)" (Right (Product (Function (Name "Marco") (Name "Paolo")) (Function (Name "Cecilia") (Name "Greta"))))
    it "parses a function between sums of products" do
      parseAdm "Marco * Paolo + Cecilia * Greta -> Francesco * Carla + Viola * Gioele"
        (Right (Function
          (Sum (Product (Name "Marco") (Name "Paolo")) (Product (Name "Cecilia") (Name "Greta")))
          (Sum (Product (Name "Francesco") (Name "Carla")) (Product (Name "Viola") (Name "Gioele")))))
    it "parses a sequence of functions" do
      parseAdm "Marco -> Paolo ; Cecilia -> Greta" (Right (Sequence (Function (Name "Marco") (Name "Paolo")) (Function (Name "Cecilia") (Name "Greta"))))
    it "parses something after an empty line" do
      parseAdm "\nMarco * Paolo + Cecilia * Greta" (Right (Sum (Product (Name "Marco") (Name "Paolo")) (Product (Name "Cecilia") (Name "Greta"))))

  describe "Assignment parser" do
    it "parses an assignment" do
      runParser assignmentAdmParser "A = b" `shouldEqual` (Right (Assignment "A" (Name "b")))
    it "parses an assignment after an empty line" do
      runParser assignmentAdmParser "\nA = b" `shouldEqual` (Right (Assignment "A" (Name "b")))

  describe "Multiple assignments parser" do
    it "parses a single assignment" do
      runParser multipleAssignmentsAdmParser "Name = Marco" `shouldEqual`
        (Right (NonEmptyList (NonEmpty (Assignment "Name" (Name "Marco")) Nil)))
    it "parses a single assignment followed by whitespace" do
      runParser multipleAssignmentsAdmParser "Name = Marco " `shouldEqual`
        (Right (NonEmptyList (NonEmpty (Assignment "Name" (Name "Marco")) Nil)))
    it "parses multiple assignments" do
      runParser multipleAssignmentsAdmParser "A = b C = d" `shouldEqual`
        (Right (NonEmptyList (NonEmpty (Assignment "A" (Name "b")) (Cons (Assignment "C" (Name "d")) Nil))))
