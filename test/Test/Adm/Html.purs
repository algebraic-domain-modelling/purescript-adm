module Test.Adm.Html where

import Adm.Adm (Assignment(..))
import Adm.Html (Html(..), admToHtml, assignmentToHtml, render)
import Adm.View (AdmView(..))

import Prelude

-- spec
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (shouldEqual)

suite :: Spec Unit
suite = describe "Html" do
  describe "admToHtml" do
    it "maps a VName to text" do
      admToHtml (VName "Marco") `shouldEqual` Div "adm-type" [Text "Marco"]
    it "maps a VHole to an empty div" do
      admToHtml VHole `shouldEqual` Div "adm-hole" []
    it "maps a VUnit to a div containing a middle dot" do
      admToHtml VUnit `shouldEqual` Div "adm-unit" [Text "&bull;"]
    it "maps a VSum to a list of divs" do
      admToHtml (VSum [VName "Marco", VName "Paolo"]) `shouldEqual`
        Div "adm-sum"
          [ Div "adm-type" [Text "Marco"]
          , Div "adm-op" [Text "+"]
          , Div "adm-type" [Text "Paolo"]
          ]
    it "maps a VProduct to a list of divs" do
      admToHtml (VProduct [VName "Marco", VName "Paolo"]) `shouldEqual`
        Div "adm-product"
          [ Div "adm-type" [Text "Marco"]
          , Div "adm-op" [Text "*"]
          , Div "adm-type" [Text "Paolo"]
          ]
    it "maps a VFunction to consecutive divs" do
      admToHtml (VFunction (VName "Marco") (VName "Paolo")) `shouldEqual`
        Div "adm-function"
          [ Div "adm-type" [Text "Marco"]
          , Div "adm-op" [Text "&#10132;"]
          , Div "adm-type" [Text "Paolo"]
          ]
    it "maps a VSequence to a list of divs" do
      admToHtml (VSequence [VName "Marco", VName "Paolo"]) `shouldEqual`
        Div "adm-sequence"
          [ Div "adm-type" [Text "Marco"]
          , Div "adm-op" [Text ";"]
          , Div "adm-type" [Text "Paolo"]
          ]
    it "maps a VWrap to a wrapped div" do
      admToHtml (VWrap "Cecilia" (VName "Paolo")) `shouldEqual`
        Div "adm-wrap"
          [ Div "adm-wrap-label" [Text "Cecilia"]
          , Div "adm-type" [Text "Paolo"]
          ]

  describe "assignmentToHtml" do
    it "maps an assignment to the correct div" do
      assignmentToHtml (Assignment "Marco" (VName "Paolo")) `shouldEqual`
        Div "adm-assignment"
          [ Div "adm-assignment-name" [Text "Marco"]
          , Div "adm-type" [Text "Paolo"]
          ]

  describe "render" do
    it "renders Text as a string" do
      render (Text "Marco") `shouldEqual` "Marco"
    it "renders Div as a div" do
      render (Div "high" [Text "Marco"]) `shouldEqual` "<div class=\"high\">Marco</div>"

