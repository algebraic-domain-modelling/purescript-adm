module Test.Adm.Adm where

import Adm.Adm (Adm(..), Assignment(..), prettyPrint)

import Prelude

-- spec
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions (shouldEqual)

suite :: Spec Unit
suite = do
  describe "Adm" do
    describe "show" do
      it "shows correctly a Name" do
        show (Name "marco") `shouldEqual` "Name marco"
      it "shows correctly a Hole" do
        show Hole `shouldEqual` "Hole"
      it "shows correctly a Unit" do
        show Unit `shouldEqual` "Unit"
      it "shows correctly a Sum" do
        show (Sum (Name "marco") (Name "paolo")) `shouldEqual` "Sum (Name marco) (Name paolo)"
      it "shows correctly a Product" do
        show (Product (Name "marco") (Name "paolo")) `shouldEqual` "Product (Name marco) (Name paolo)"
      it "shows correctly a Function" do
        show (Function (Name "marco") (Name "paolo")) `shouldEqual` "Function (Name marco) (Name paolo)"
      it "shows correctly a Sequence" do
        show (Sequence (Name "marco") (Name "paolo")) `shouldEqual` "Sequence (Name marco) (Name paolo)"
      it "shows correctly a Wrap" do
        show (Wrap "paolo" (Name "marco")) `shouldEqual` "Wrap paolo (Name marco)"

    describe "prettyPrint" do
      it "prints correctly a Name" do
        prettyPrint (Name "marco") `shouldEqual` "marco"
      it "prints correctly a Hole" do
        prettyPrint Hole `shouldEqual` "_"
      it "prints correctly a Unit" do
        prettyPrint Unit `shouldEqual` "·"
      it "prints correctly a Sum" do
        prettyPrint (Sum (Name "marco") (Name "paolo")) `shouldEqual` "(marco) + (paolo)"
      it "prints correctly a Product" do
        prettyPrint (Product (Name "marco") (Name "paolo")) `shouldEqual` "(marco) * (paolo)"
      it "prints correctly a Function" do
        prettyPrint (Function (Name "marco") (Name "paolo")) `shouldEqual` "(marco) -> (paolo)"
      it "prints correctly a Sequence" do
        prettyPrint (Sequence (Name "marco") (Name "paolo")) `shouldEqual` "(marco) ; (paolo)"
      it "prints correctly a Wrap" do
        prettyPrint (Wrap "paolo" (Name "marco")) `shouldEqual` "paolo (marco)"

  describe "Assignment" do
    describe "show" do
      it "shows correctly an Assignment" do
        show (Assignment "marco" (Name "paolo")) `shouldEqual` "Assignment marco (Name paolo)"
