module Adm.Adm where

import Prelude

data Adm
  = Name String
  | Hole
  | Unit
  | Sum Adm Adm
  | Product Adm Adm
  | Function Adm Adm
  | Sequence Adm Adm
  | Wrap String Adm

derive instance eqAdm :: Eq Adm

instance showAdm :: Show Adm where
  show (Name s)       = "Name " <> s
  show Hole           = "Hole"
  show Unit           = "Unit"
  show (Sum      x y) = "Sum ("      <> show x <> ") (" <> show y <> ")"
  show (Product  x y) = "Product ("  <> show x <> ") (" <> show y <> ")"
  show (Function x y) = "Function (" <> show x <> ") (" <> show y <> ")"
  show (Sequence x y) = "Sequence ("  <> show x <> ") (" <> show y <> ")"
  show (Wrap s adm)   = "Wrap " <> s <> " (" <> show adm <> ")"

prettyPrint :: Adm -> String
prettyPrint (Name s)       = s
prettyPrint Hole           = "_"
prettyPrint Unit           = "·"
prettyPrint (Sum      x y) = "(" <> prettyPrint x <> ") + ("  <> prettyPrint y <> ")"
prettyPrint (Product  x y) = "(" <> prettyPrint x <> ") * ("  <> prettyPrint y <> ")"
prettyPrint (Function x y) = "(" <> prettyPrint x <> ") -> (" <> prettyPrint y <> ")"
prettyPrint (Sequence x y) = "(" <> prettyPrint x <> ") ; (" <> prettyPrint y <> ")"
prettyPrint (Wrap s adm)   = s <> " (" <> prettyPrint adm <> ")"

data Assignment a = Assignment String a

derive instance eqAssignment :: Eq a => Eq (Assignment a)

instance showAssignment :: Show a => Show (Assignment a) where
  show (Assignment s adm) = "Assignment " <> s <> " (" <> show adm <> ")"

derive instance functorAssignment :: Functor Assignment
