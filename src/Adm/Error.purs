module Adm.Error where

import Prelude

-- exceptions
import Effect.Exception (Error, error)

-- string-parser
import Text.Parsing.StringParser (ParseError(..), Pos)

data AdmError = ParserError ParseError Pos

fromUnparserError :: { error :: ParseError, pos :: Pos } -> AdmError
fromUnparserError { error, pos } = ParserError error pos

errorMessage :: AdmError -> String
errorMessage (ParserError (ParseError parseError) pos) = "Parse error \"" <> parseError <>"\" at position " <> show pos

toError :: AdmError -> Error
toError = error <<< errorMessage
