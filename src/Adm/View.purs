module Adm.View where

import Adm.Adm (Adm(..))

import Prelude

-- foldable-traversable
import Data.Foldable (intercalate)

data AdmView
  = VName String
  | VHole
  | VUnit
  | VSum (Array AdmView)
  | VProduct (Array AdmView)
  | VFunction AdmView AdmView
  | VSequence (Array AdmView)
  | VWrap String AdmView

derive instance eqAdmView :: Eq AdmView

instance showAdmView :: Show AdmView where
  show (VName s)            = "VName " <> s
  show VHole                = "VHole"
  show VUnit                = "VUnit"
  show (VSum      summands) = "VSum ("      <> intercalate ", " (show <$> summands) <> ")"
  show (VProduct  factors ) = "VProduct ("  <> intercalate ", " (show <$> factors ) <> ")"
  show (VFunction x y)      = "VFunction (" <> show x <> ") (" <> show y <> ")"
  show (VSequence terms   ) = "VSequence (" <> intercalate "; " (show <$> terms ) <> ")"
  show (VWrap s adm)        = "VWrap " <> s <> " (" <> show adm <> ")"

view :: Adm -> AdmView
view (Name s) = VName s
view Hole = VHole
view Unit = VUnit
view (Sum x y) = case view x of
  VSum xs -> case view y of
    VSum ys -> VSum (xs  <> ys  )
    y'      -> VSum (xs  <> [y'])
  x'      -> case view y of
    VSum ys -> VSum ([x'] <> ys )
    y'      -> VSum ([x', y'])
view (Product x y) = case view x of
  VProduct xs -> case view y of
    VProduct ys -> VProduct (xs  <> ys  )
    y'          -> VProduct (xs  <> [y'])
  x'          -> case view y of
    VProduct ys -> VProduct ([x'] <> ys )
    y'          -> VProduct ([x', y'])
view (Function x y) = VFunction (view x) (view y)
view (Sequence x y) = case view x of
  VSequence xs -> case view y of
    VSequence ys -> VSequence (xs  <> ys )
    y'           -> VSequence (xs  <> [y'])
  x'           -> case view y of
    VSequence ys -> VSequence ([x'] <> ys)
    y'           -> VSequence ([x', y'])
view (Wrap s x) = VWrap s (view x)
