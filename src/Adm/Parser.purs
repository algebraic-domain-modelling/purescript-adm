module Adm.Parser where

import Adm.Adm (Adm(..), Assignment(..))

import Prelude hiding (between)

-- control
import Control.Alternative ((<|>))

-- foldable-traversable
import Data.Foldable (class Foldable, foldMap)

-- lists
import Data.List.NonEmpty (toList)
import Data.List.Types (NonEmptyList)

-- strings
import Data.String.CodeUnits (singleton)

-- string-parsers
import Text.Parsing.StringParser (Parser, fail, try)
import Text.Parsing.StringParser.CodePoints (alphaNum, skipSpaces, string)
import Text.Parsing.StringParser.Combinators (between, fix, many1, sepEndBy1)
import Text.Parsing.StringParser.Expr (Assoc(..), Operator(..), buildExprParser)


-- helpers
fromChars :: forall f. Foldable f => f Char -> String
fromChars = foldMap singleton

wordParser :: Parser String
wordParser = fromChars <<< toList <$> many1 alphaNum

parens :: forall a. Parser a -> Parser a
parens p = between (string "(" <* skipSpaces) (skipSpaces *> string ")") p

notFollowedBy :: forall a. Parser a -> Parser Unit
notFollowedBy p = try ((try p *> fail "Negated parser succeeded") <|> pure unit)

-- term parsers
nameParser :: Parser Adm
nameParser = Name <$> wordParser <* notFollowedBy (skipSpaces *> string "=")

holeParser :: Parser Adm
holeParser = string "_" *> pure Hole

unitParser :: Parser Adm
unitParser = string "1" *> pure Unit

wrapParser :: Parser Adm -> Parser Adm
wrapParser p = Wrap <$> (wordParser <* skipSpaces) <*> p

termParser :: Parser Adm -> Parser Adm
termParser p
  =   holeParser
  <|> unitParser
  <|> try (parens p)
  <|> try (wrapParser holeParser)
  <|> try (wrapParser unitParser)
  <|> try (wrapParser $ parens p)
  <|> try (wrapParser nameParser)
  <|> nameParser

-- operations parsers
sumParser :: Parser (Adm -> Adm -> Adm)
sumParser = try $ Sum <$ (skipSpaces *> string "+" <* skipSpaces)

productParser :: Parser (Adm -> Adm -> Adm)
productParser = try $ Product <$ (skipSpaces *> string "*" <* skipSpaces)

functionParser :: Parser (Adm -> Adm -> Adm)
functionParser = try $ Function <$ (skipSpaces *> string "->" <* skipSpaces)

sequenceParser :: Parser (Adm -> Adm -> Adm)
sequenceParser = try $ Sequence <$ (skipSpaces *> string ";" <* skipSpaces)

-- adm parser
admParser :: Parser Adm
admParser = fix $ \p -> skipSpaces *>
  buildExprParser
    [ [Infix productParser  AssocRight]
    , [Infix sumParser      AssocRight]
    , [Infix functionParser AssocRight]
    , [Infix sequenceParser AssocRight]
    ]
    (termParser p)

-- assignment parser
assignmentAdmParser :: Parser (Assignment Adm)
assignmentAdmParser = do
  _ <- skipSpaces
  name <- wordParser
  _ <- skipSpaces *> string "=" <* skipSpaces
  adm <- admParser
  pure $ Assignment name adm

multipleAssignmentsAdmParser :: Parser (NonEmptyList (Assignment Adm))
multipleAssignmentsAdmParser = sepEndBy1 assignmentAdmParser skipSpaces
