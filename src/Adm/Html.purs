module Adm.Html where

import Adm.Adm (Assignment(..))
import Adm.View (AdmView(..))

import Prelude

-- foldable-traversable
import Data.Foldable (foldMap, intercalate)

data Html
  = Text String
  | Div String (Array Html)

derive instance eqHtml :: Eq Html

instance showHtml :: Show Html where
  show (Text s)         = "Text " <> s
  show (Div s children) = "Div " <> s <> " [" <> foldMap show children <> "]"

admToHtml :: AdmView -> Html
admToHtml (VName s)          = Div "adm-type" [Text s]
admToHtml VHole              = Div "adm-hole" []
admToHtml VUnit              = Div "adm-unit" [Text "&bull;"]
admToHtml (VSum summands)    = Div "adm-sum" $
  intercalate
    [Div "adm-op" [Text "+"]]
    (pure <<< admToHtml <$> summands)
admToHtml (VProduct factors) = Div "adm-product" $
  intercalate
    [Div "adm-op" [Text "*"]]
    (pure <<< admToHtml <$> factors)
admToHtml (VFunction x y)    = Div "adm-function"
  [ admToHtml x
  , Div "adm-op" [Text "&#10132;"]
  , admToHtml y
  ]
admToHtml (VSequence terms) = Div "adm-sequence" $
  intercalate
    [Div "adm-op" [Text ";"]]
    (pure <<< admToHtml <$> terms)
admToHtml (VWrap s x)        = Div "adm-wrap"
  [ Div "adm-wrap-label" [Text s]
  , admToHtml x
  ]

assignmentToHtml :: Assignment AdmView -> Html
assignmentToHtml (Assignment name adm) = Div "adm-assignment"
  [ Div "adm-assignment-name" [Text name]
  , admToHtml adm
  ]

render :: Html -> String
render (Text s) = s
render (Div className children)
  =  "<div class=\""  <> className <> "\">"
  <> foldMap render children
  <> "</div>"
