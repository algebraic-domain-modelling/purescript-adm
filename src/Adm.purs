module Adm where

import Adm.Error (AdmError, fromUnparserError, toError)
import Adm.Html (assignmentToHtml, render)
import Adm.Parser (multipleAssignmentsAdmParser)
import Adm.View (view)

import Prelude

-- bifunctors
import Data.Bifunctor (bimap)

-- effect
import Effect (Effect)

-- either
import Data.Either (Either, either)

-- exceptions
import Effect.Exception (throwException)

-- foldable-traversable
import Data.Semigroup.Foldable (fold1)

-- string-parsers
import Text.Parsing.StringParser (unParser)

adm' :: String -> Either AdmError String
adm' s =
  let
    unparserResult = unParser multipleAssignmentsAdmParser { pos: 0, str: s }
    mappedResult = bimap fromUnparserError (\a -> a.result) unparserResult
  in
    fold1 <$> map (render <<< assignmentToHtml <<< (map view)) <$> mappedResult

adm :: String -> Effect String
adm s = either (throwException <<< toError) pure (adm' s)
