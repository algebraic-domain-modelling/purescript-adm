# purescript-adm

A small Purescript package to parse an `algebraic domain modelling` data type and rendering it as HTML.

## build

Build it using `npm run build`.

You could rebuild automatically at every file save using `npm run build -- --watch`

## test

Run tests using `npm run test`.

You could rerun the tests automatically at every file save using `npm run test -- --watch`

## bundle

Bundle up the module using `npm run bundle` and find the output in `index.js`.

## bundle-app

Bundle up the module using `npm run bundle-app` and then open `html-dist/index.html` to interact in the browser.
