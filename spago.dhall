{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "purescript-adm"
, dependencies =
  [ "effect"
  , "foldable-traversable"
  , "lists"
  , "psci-support"
  , "smolder"
  , "string-parsers"
  , "strings"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs" ]
}
